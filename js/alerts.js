class Alerts {
  static loadAlerts() {
    let alertsSection = document.getElementById('alerts');
    let claimsSection = document.getElementById('claims');
    let chatbotSection = document.getElementById('chatbot');
    alertsSection.style.display = "flex";
    claimsSection.style.display = "none";
    chatbotSection.style.display = "none";
    alertsBtn.style.backgroundColor = 'lightgray';
    chatbotBtn.style.backgroundColor = 'white';
    claimsBtn.style.backgroundColor = 'white';
    alertsSection.innerHTML = '';
    for (let i = 0; i < alerts.length; i++) {
      alertsSection.innerHTML +=`<div class="alert-item"><div>${alerts[i]['alert']}</div><div>${alerts[i]["redeemed-by"]}</div></div>`
    }
  }
}
