const alertsBtn = document.getElementById('alerts-btn');
const claimsBtn = document.getElementById('claims-btn');
const chatbotBtn = document.getElementById('chatbot-btn');
const alerts = [
  {
    'alert': 'Alerta 1',
    'redeemed-by': 'Usuario 1'
  },
  {
    'alert': 'Alerta 2',
    'redeemed-by': 'Usuario 1'
  },
  {
    'alert': 'Alerta 1',
    'redeemed-by': 'Usuario 2'
  },
]

const messages = [
  {
    'user': 'user1',
    'message': 'Hello from TW'
  },
  {
    'user': 'user2',
    'message': 'Hello from FB'
  },
  {
    'user': 'user1',
    'message': 'Nice channel! I love the emotes'
  },
  {
    'user': 'user3',
    'message': 'Hello from YT'
  },
]

const socket = io("http://localhost:23000", {
  path: "/redeem-alerts/"
});

